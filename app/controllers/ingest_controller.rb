class IngestController < ApplicationController
  
  skip_before_action :verify_authenticity_token

  # POST /ingest/:event_slug
  def ingest
    @event = Conference.where(slug: params[:event_slug]).first

    parts = JSON.parse(request.raw_post)

    firstname = parts['first-name']
    secondname = parts['second-name']
    pronoun = parts['pronoun']
    email = parts['your-email']
    role = parts['role'].join(',')
    company = parts['company']
    country = parts['country']

    registration = Registration.new
    registration.location = country
    registration.activity = role
    registration.pronoun = pronoun
    registration.conference = @event.id
    registration.email = email
    registration.company = company
    registration.firstname = firstname
    registration.surname = secondname

    registration.save!
    puts registration
  end

end
