class RegisterController < ApplicationController
  
  def index
    if !session.has_key?(:user_id)
      cookies[:return_to] = request.url
      redirect_to "/b/ldap_signin"
      return
    end

    email = User.where(id: session[:user_id]).first.email

    @activeconferences = Conference.where(registration: true).order('id asc')
    @conferenceadmin = {}
    @activeconferences.each do |conf|
      @conferenceadmin[conf.id] = false
      conf.admins.to_s.split(',').each do |admin|
        if session[:user_id].to_s == admin
          @conferenceadmin[conf.id] = true
        end
      end
    end

    @registrations = {}

    Registration.where(user: session[:user_id]).each do |registration|
      @registrations[registration.conference] = true
    end

    Registration.where(email: email).each do |registration|
      @registrations[registration.conference] = true
    end

    @alert = false
    if params[:success]
      registered_event = Conference.find(params[:success])
      @alert = "You have now registered for #{registered_event.name}"
    end
    if params[:update]
      registered_event = Conference.find(params[:update])
      @alert = "Thank You. You have updated your registered for #{registered_event.name}"
    end
  end

  def eventinfo
    @event = Conference.where(slug: params[:event_slug]).first
  end
  
  def event
    @event = Conference.where(slug: params[:event_slug]).first

    if !session.has_key?(:user_id)
      cookies[:return_to] = request.url
      redirect_to "/b/ldap_signin"
      return
    end

    email = User.where(id: session[:user_id]).first.email

    @registration = Registration.where(user: session[:user_id], conference: @event.id).first
    if !@registration then
      @registration = Registration.where(email: email, conference: @event.id).first
    end

    @roles = []
    if(@registration && @registration.activity)
      @roles = @registration.activity.split(",")
    end

    if request.post?
      new_registration = false
      
      if(!@registration)
        @registration = Registration.new
        @registration.user = session[:user_id]
        @registration.conference = @event.id
        
        new_registration = true
      end

      if params[:location] then
        @registration.location = params[:location]
      end
      
      if params[:roles] then
        @registration.activity = params[:roles].values.join(",")
      end

      if params[:pronoun]
        @registration.pronoun = params[:pronoun]
      end

      if params['firstname'] then
        @registration.firstname = params['firstname']
      end

      if params['surname'] then
        @registration.surname = params['surname']
      end

      @registration.email = email

      if params['role'] then
        @registration.activity = params['role'].join(',')
      end

      if params['company'] then
        @registration.company = params['company']
      end

      if params['roomslot'] then
        @registration.roomslot = params['roomslot']
      end

      if params['country'] then
        @registration.location = params['country']
      end

      if @event.slug == "akademy2020" then
        if params['photoconsent'] then
          @registration.photoconsent = 1
        else
          @registration.photoconsent = 0
        end
      end

      if @event.slug == "postakademy2020" then
        if params['secondescape'] then
          @registration.secondescape = 1
        else
          @registration.secondescape = 0
        end
      end

      if params['photoname'] then
        @registration.photoname = params['photoname']
      end

      if params['photonumber'] then
        @registration.photonumber = params['photonumber']
      end

      @registration.save!

      if new_registration
        redirect_to "/b/register?success=#{@event.id}"
      else
        redirect_to "/b/register?update=#{@event.id}"
      end
    end
  end

  def admin
    @event = Conference.where(slug: params[:event_slug]).first

    @saved = false
    if request.post?
      @event.info = params[:info]
      @event.save!
      @saved = true
    end
    
    access = false
    @event.admins.to_s.split(',').each do |admin|
      if session[:user_id].to_s == admin
        access = true
      end
    end
    if !access
      return redirect_to "/b"
    end

    if @event.slug != 'escape2020' then
      @countries = {}
      Registration.where(conference: @event.id).pluck('location').uniq.each do |answer|
        if !@countries[answer.gsub(/\"*\]*\[*/, '')] then
          @countries[answer.gsub(/\"*\]*\[*/, '')] = 0
        end
        @countries[answer.gsub(/\"*\]*\[*/, '')] = @countries[answer.gsub(/\"*\]*\[*/, '')] + Registration.where(location: answer.gsub(/\"*\]*\[*/, ''), :conference => @event.id).count
      end

      @pronouns = {}
      Registration.where(conference: @event.id).pluck('pronoun').uniq.each do |answer|
        if !@pronouns[answer.gsub(/\"*\]*\[*\\*/, '')] then
          @pronouns[answer.gsub(/\"*\]*\[*\\*/, '')] = 0
        end
        @pronouns[answer.gsub(/\"*\]*\[*/, '')] = @pronouns[answer.gsub(/\"*\]*\[*/, '')] + Registration.where(pronoun: answer.gsub(/\"*\]*\[*/, ''), :conference => @event.id).count
      end

      @roles = {}
      Registration.where(conference: @event.id).pluck('activity').each do |answer|
        if answer != nil
          roles = answer.split(',')
          roles.each do |role|
            if !@roles[role].present?
              @roles[role] = 1
            else
              @roles[role] = @roles[role] + 1
            end
          end
        end
      end
    end

    @registrants = []
    users = {}
    User.where(id: Registration.where(conference: @event.id).pluck('user')).each do |user|
      users[user.id] = user
    end
    Registration.where(conference: @event.id).each do |registrant|
      if @event.slug == 'kdab2020' then
        @registrants.push({
          :name => "#{registrant.firstname} #{registrant.surname}",
          # :username => users[registrant.user].username,
          :email => registrant.email,
          :company => registrant.company,
	        :country => registrant.location
        })
      else
        @registrants.push({
          :name => users[registrant.user].name,  
          :username => users[registrant.user].username,
          :email => users[registrant.user].email,
          :company => registrant.company,
          :country => registrant.location,
          :roomslot => registrant.roomslot,
          :photoconsent => registrant.photoconsent,
          :photoname => registrant.photoname,
          :photonumber => registrant.photonumber
        })
      end
    end
  end
end

