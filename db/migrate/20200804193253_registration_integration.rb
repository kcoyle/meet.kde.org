class RegistrationIntegration < ActiveRecord::Migration[5.2]
  def change
    add_column :registrations, :email, :string
    add_column :registrations, :company, :string
    add_column :registrations, :firstname, :string
    add_column :registrations, :surname, :string
  end
end
