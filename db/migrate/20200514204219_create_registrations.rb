class CreateRegistrations < ActiveRecord::Migration[5.2]
  def change
    create_table :registrations do |t|
      t.string :location
      t.string :activity
      t.string :employment
      t.text :social

      t.timestamps
    end
  end
end
