class ActiveConference < ActiveRecord::Migration[5.2]
  def change
    add_column :conferences, :active, :boolean
  end
end
