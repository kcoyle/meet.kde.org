class RegistrationUser < ActiveRecord::Migration[5.2]
  def change
    add_column :registrations, :conference, :integer
    add_column :registrations, :user, :integer
  end
end
