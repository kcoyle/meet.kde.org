class Eventinfo < ActiveRecord::Migration[5.2]
  def change
    add_column :conferences, :info, :string
  end
end
