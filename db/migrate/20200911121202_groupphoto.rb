class Groupphoto < ActiveRecord::Migration[5.2]
  def change
    add_column :registrations, :photoconsent, :integer
    add_column :registrations, :photoname, :string
    add_column :registrations, :photonumber, :string
    add_column :registrations, :secondescape, :integer
  end
end
