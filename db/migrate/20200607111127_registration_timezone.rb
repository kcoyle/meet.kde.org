class RegistrationTimezone < ActiveRecord::Migration[5.2]
  def change
    add_column :registrations, :pronoun, :string
    add_column :registrations, :timezone, :string
  end
end
