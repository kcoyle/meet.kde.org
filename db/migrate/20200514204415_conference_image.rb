class ConferenceImage < ActiveRecord::Migration[5.2]
  def change
    add_column :conferences, :image, :string
  end
end
