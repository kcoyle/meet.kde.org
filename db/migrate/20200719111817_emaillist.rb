class Emaillist < ActiveRecord::Migration[5.2]
  def change
    create_table :emaillist do |t|
      t.integer :conference
      t.integer :userid
      t.boolean :registered

      t.timestamps
    end

    add_column :conferences, :emaillist, :string
  end
end
