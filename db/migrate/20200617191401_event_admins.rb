class EventAdmins < ActiveRecord::Migration[5.2]
  def change
    add_column :conferences, :admins, :string
  end
end
