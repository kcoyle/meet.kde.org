namespace :registration do
  task emaillist: :environment do
    puts "======== Mailing List :: Start ========"

    Conference.where("length(emaillist) > 3").each do |conference|
      puts "== Conference :: #{conference.name}"

      Registration.where(:conference => conference.id).each do |registration|
        if Emaillist.where(:userid => registration.user, :conference => conference.id).count == 0
          user = User.find(registration.user)
          puts "== == Registering #{user.name} :: #{user.email} :: https://mail.kde.org/mailman/subscribe/akademy-attendees?email=#{user.email}"

          record = Emaillist.new
          record.conference = conference.id
          record.userid = user.id
          record.registered = true
          record.save
        end
      end
    end

    puts "======== Mailing List :: End ========"
  end

end
